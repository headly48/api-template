API - Template

This project is designed as a template to quickly and easily create additional API's in the future

Includes Spring, Swagger, Liquibase


INSTALLATION

Install gradle 2.1 http://www.gradle.org/  

Create a schema 'template' and a user with the password/username set as 'template'

Run the command 'gradle update' to create the database tables

run 'gradle build' to generate the war and then deploy this on a tomcat8 server with the context template



TODO's
Add Tests (Unit/Functional/integration) and any required frameworks
Look into error handling (generic error scenarios) 
Look into HealthCheckAuthKey and best way to implement
Change the layout of the packages
Exclude the config file from war task in order to externalize it 
Change build.gradle file to get dependency versions from config
Review the build.gradle file to remove any unnecessary/duplicate dependencies






