package au.com.headly.template.services.response.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class HealthCheck extends Response {
	
	private List<TableHealthCheck> tableHealthCheck;	
	
	private String version;
	private String applicationName;
	
	public HealthCheck(List<TableHealthCheck> tableStatus, Status status, String version, String appName){
		
		super(status);
		this.tableHealthCheck = tableStatus;
		this.version = version;
		this.applicationName = appName;
	}
	
	public List<TableHealthCheck> getTableHealthCheck(){
		
		return tableHealthCheck;
	}
	
	public String getVersion(){
		
		return version;
	}
	
	public String getApplicationName(){
		
		return applicationName;
	}
}
