package au.com.headly.template.services.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.metamodel.EntityType;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Component;

import au.com.headly.template.services.dao.AdminDao;
import au.com.headly.template.services.response.dto.TableHealthCheck;

@Component
@Transactional
public class AdminDaoImpl implements AdminDao {

    @PersistenceContext
    private EntityManager entityManager;

	@Override
	public List<TableHealthCheck> getTableStatus() {
		
		List<TableHealthCheck> tableStatus = new ArrayList<TableHealthCheck>();
		
		for (EntityType<?> entity : entityManager.getMetamodel().getEntities()) {
		    final String className = entity.getName();

		    Query q = entityManager.createQuery("from " + className + " c");
		    
		    tableStatus.add(new TableHealthCheck(className, q.getResultList().size()));
		}		

		return tableStatus;
	}  
}
