package au.com.headly.template.services.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "template")
@Entity
public class Template {
	
    @Id
    @GeneratedValue
	private String id;
    
	private String templateMessage;
	
	public Template(String templateMessage){
		
		this.templateMessage = templateMessage;
	}
	
	public Template(){		

	}
	
	public String getTemplateMessage(){
		return templateMessage;
	}
	
	public void setTemplateMessage(String templateMessage){
		this.templateMessage = templateMessage;
	}
}
