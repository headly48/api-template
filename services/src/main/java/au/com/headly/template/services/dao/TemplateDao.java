package au.com.headly.template.services.dao;

import java.util.List;

import au.com.headly.template.services.domain.Template;

public interface TemplateDao {

	public void addTemplateRecord(String templateMessage);
	
	public List<Template> getTemplateRecords();

}