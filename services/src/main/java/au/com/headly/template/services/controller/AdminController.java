package au.com.headly.template.services.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.headly.template.services.dao.AdminDao;
import au.com.headly.template.services.dao.TemplateDao;
import au.com.headly.template.services.response.dto.HealthCheck;
import au.com.headly.template.services.response.dto.Response;
import au.com.headly.template.services.response.dto.Status;
import au.com.headly.template.services.response.dto.TableHealthCheck;

@Controller
public class AdminController {
	
    @Autowired
    private TemplateDao templateDao;
    
    @Autowired
    private AdminDao adminDao;  
    
	@Value("${api.title}")
	private String appName;

	@Value("${healthcheck.expected.number.tables}")
	private int expectedNumberOfTables;

	@Value("${api.version}")
	private String version;	    

	@Value("${healthcheck.security.authKey}")
    private String healthCheckAuthKey = "PIUM-DIWD-MQGY";

    @RequestMapping(value = "/v1/healthcheck", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Response getHealthcheck(HttpServletRequest httpRequest) {    	
    	
    	String authKey = httpRequest.getHeader("healthAuthToken");
    	
    	List<TableHealthCheck> tableStatus = null;
    	Status status = Status.Success;    	
    	
    	if(StringUtils.equals(healthCheckAuthKey, authKey)){
    		tableStatus = adminDao.getTableStatus();
    		
        	if(tableStatus.size() != expectedNumberOfTables){
        		status = Status.Failed;       		
        	}	      		
    	}   
    	
    	return new HealthCheck(tableStatus, status, version, appName);	    	
    }	
}