package au.com.headly.template.services.response.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Response {

	private Status status;		
	private Error error;
	
	public Response(Status status){
		this.status = status;
	}
	
	public Status getStatus(){
		
		return status;
	}
	
	public void setError(Error error){
		
		this.error = error;
	}
	
	public Error getError(){
		
		return error;
	}
}
