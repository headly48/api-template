package au.com.headly.template.services.dao.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;

import org.springframework.stereotype.Component;

import au.com.headly.template.services.dao.TemplateDao;
import au.com.headly.template.services.domain.Template;

@Component
@Transactional
public class TemplateDaoImpl implements TemplateDao {

    @PersistenceContext
    private EntityManager entityManager;
    
	@Override
	public void addTemplateRecord(String templateMessage) {
		
		Template template = new Template(templateMessage);
		
		entityManager.persist(template);
		
	}

	@Override
	public List<Template> getTemplateRecords() {
				
		List<Template> templates = entityManager.createNativeQuery("Select * from template").getResultList();
		return templates;
	}
}
