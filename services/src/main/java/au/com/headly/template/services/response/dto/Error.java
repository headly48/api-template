package au.com.headly.template.services.response.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Error {

	private String code;	
	private String description;
	
	public Error(String description, String code){
		
		this.description = description;
		this.code = code;
	}
	
	public Error(String description){
		
		this.description = description;
	}
	
	public String getCode(){
		
		return code;
	}
	
	public String getDescription(){
		
		return description;
	}
}
