package au.com.headly.template.services.dao;

import java.util.List;

import au.com.headly.template.services.domain.Template;
import au.com.headly.template.services.response.dto.TableHealthCheck;

public interface AdminDao {
	
	public List<TableHealthCheck> getTableStatus();

}