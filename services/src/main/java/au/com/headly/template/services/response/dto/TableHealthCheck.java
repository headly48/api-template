package au.com.headly.template.services.response.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class TableHealthCheck {

	private String tableName;	
	private int rowCount;
	
	public TableHealthCheck(String tableName, int rowCount){
		this.rowCount = rowCount;
		this.tableName = tableName;
	}
	
	public String getTableName(){
		
		return tableName;
	}
	
	public int getRowCount(){
		
		return rowCount;
	}
}
