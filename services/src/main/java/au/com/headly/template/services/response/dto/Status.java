package au.com.headly.template.services.response.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

public enum Status {

	Success, Failed
}
