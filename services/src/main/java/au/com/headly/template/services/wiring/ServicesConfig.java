package au.com.headly.template.services.wiring;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({ au.com.headly.template.services.persistence.PersistenceConfig.class})
@ComponentScan(basePackageClasses = { au.com.headly.template.services.controller.ComponentScanningMarker.class, au.com.headly.template.services.dao.ComponentScanningMarker.class})
public class ServicesConfig {

	
}